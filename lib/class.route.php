<?php
namespace routes;
use smartUrl\smartUrl as smartUrl;

class routes extends smartUrl {

    private $route = [];
    private $applicationRoute = [];
    private $inputParm = [];

    // load the route ini file
    public function configIni($path,$iniFile){
        // set the ini file to an sections array
        $ini_array = parse_ini_file($path.'/'.$iniFile, true);
        // set the correct pathe on the load array
        $ini_array['load']['path'] = $path;
        $this->setRoute($ini_array);
    }

    //set the ini file data to an array route
    private function setRoute($ini_array){

        $buildRoute = [
            'load' => $ini_array['load'],
            'route' => [],
        ];

        foreach ($ini_array as $key => $arr) {
            if($key !== 'load'){
                // array merge the load section whit each route section
                $buildRoute['route'][] = array_merge(['name'=>$key],$arr);
            }
        }

        $name = $ini_array['load']['name'];
        $this->route[$name] = $buildRoute;
    }

    // load all routes that has a correct matches whit the url
    public function getApplicationRoutes(){

        foreach ($this->route as $module => $arr) {
            // compaire each found route whit the request
            $allowedRoute = $this->matchApplicationMethod($arr);
            // if there is a match build the application route
            if($allowedRoute){
                $this->applicationRoute = $allowedRoute;
            }
        }
        return $this->applicationRoute;
    }

    // compaire each found route whit the request
    private function matchApplicationMethod($applcationIni){
        $appRoute = $applcationIni['route'];
        $appLoad = $applcationIni['load'];

        $applicationRoute = [];
        foreach ($appRoute as $key => $route) {
            // make sure the route has the same method as the request method
            if (in_array($this->getMethod(), $route['method'])) {
                //return each found route that matches the request
                if($this->matchApplicationParm($route['param'])){
                    $route['page'] = $appLoad['path'] . $route['page'] ;
                    $this->applicationRoute[] = $route;
                }
            }
        }
        return $this->applicationRoute;
    }

    //return each found route that matches the request
    private function matchApplicationParm($appParm){
        $match = true;
        foreach ($appParm as $key => $parm) {
            // check if the route parmeter is a dynamic value
            $posColon = strpos($parm, ':');

            // check the none dynamic value on a exact match
            if ($posColon === false && $this->parameter[$key] !== $parm) {
                $match = false;
                continue;
            }

            // check if the dynamic value is mandatory by #true
            $posHex = strpos($parm, '#');
            if ($posHex === false) {
                // dynamic value is not mandatory we not check if the value is set
                $strParm = substr($parm, $posColon + 1);
            } else {
                $strParm = substr($parm, $posColon + 1, $posHex - 1);
                $strHex = substr($parm, $posHex + 1);
                // dynamic value is mandatory we also check if the value is set
                if ($strHex === 'true' && empty($this->parameter[$key])) {
                    $match = false;
                }
            }
            // set all dynamic var in the inputParm value
            $this->inputParm[$strParm] = (isset($this->parameter[$key]) ? $this->parameter[$key] : null);
        }
        return $match;
    }

    // get the found dynamic var inside the request url
    public function getInput($name){
        return (isset($this->inputParm[$name]) ?  $this->inputParm[$name] :null);
    }

};


